<?php

	abstract class Storage {
    abstract public function create($object);
    abstract public function read($id);
    abstract public function update($id, $object);
    abstract public function delete($id);
    abstract public function list();
}

	abstract class View {
    protected $storage;

    public function __construct($storage) {
        $this->storage = $storage;
    }

    abstract public function displayTextById($id);
    abstract public function displayTextByUrl($url);
}

	abstract class User {
    protected $id;
    protected $name;
    protected $role;

    abstract public function getTextsToEdit();
}

	class FileStorage extends Storage {
    public function create($object) {
        $slug = $object->slug;
        $date = date('Y-m-d');

        if (file_exists($slug . '_' . $date)) {
            $i = 1;
            while (file_exists($slug . '_' . $date . '_' . $i)) {
                $i++;
            }

            $slug = $slug . '_' . $date . '_' . $i;
            $object->slug = $slug;
        } else {
            $slug = $slug . '_' . $date;
            $object->slug = $slug;
        }

        file_put_contents($slug, serialize($object));

        return $slug;
    }

    public function read($id) {
        return unserialize(file_get_contents($id));
    }

    public function update($id, $object) {
        file_put_contents($id, serialize($object));

        return true;
    }

    public function delete($id) {
        unlink($id);

        return true;
    }

    public function list() {
        $files = scandir('.');

        $texts = [];

        foreach ($files as $file) {
            if (strpos($file, '.txt') !== false) {
                array_push($texts, unserialize(file_get_contents($file)));
            }
        }

        return $texts;
    }
}